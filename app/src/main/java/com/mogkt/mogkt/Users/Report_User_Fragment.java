package com.mogkt.mogkt.Users;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.mogkt.mogkt.R;

import java.util.Calendar;

/**
 * Created by alper_yilmaz_111 on 3.03.2018.
 */

public class Report_User_Fragment extends Fragment {

    TextView tw_user, tw2_user;
    Button btn_user_baslangic, btn_user_bitis;
    int year, month, day;
    Calendar calendar;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_user_report, null);

        tw_user = (TextView) view.findViewById(R.id.tw_user_baslangic);
        btn_user_baslangic = (Button) view.findViewById(R.id.user_baslangic_tarih);
        tw2_user = (TextView) view.findViewById(R.id.tw_user_bitis);
        btn_user_bitis = (Button) view.findViewById(R.id.user_bitis_tarih);
        calendar = Calendar.getInstance();
        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);

        btn_user_baslangic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        month = month+1;
                        tw_user.setText(dayOfMonth+"/"+month+"/"+year);

                    }
                },year,month,day);
                datePickerDialog.show();

            }
        });

        btn_user_bitis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        month = month+1;
                        tw2_user.setText(dayOfMonth+"/"+month+"/"+year);

                    }
                },year,month,day);
                datePickerDialog.show();

            }
        });
        return view;

    }
}
