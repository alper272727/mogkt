package com.mogkt.mogkt.Users.Exit;

import android.Manifest;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mogkt.mogkt.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by alper_yilmaz_111 on 3.03.2018.
 */

public class User_Home_Fragment_Exit extends Fragment implements LocationListener {

    private TextView textView_user;
    private LocationManager locationManager;
    private Button btnExit_User, btnEnter_User;
    ProgressBar progressBar;
    DatabaseReference databaseReference, databaseReferenceExit;
    FirebaseAuth.AuthStateListener authStateListener;
    FirebaseAuth firebaseAuth;
    ImageButton imageButton;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_user_home_exit, null);

        databaseReference = FirebaseDatabase.getInstance().getReference("exitTable");



        firebaseAuth = FirebaseAuth.getInstance();

        imageButton = (ImageButton) view.findViewById(R.id.btnExit_User_Exit);
        textView_user = (TextView) view.findViewById(R.id.lastEnter_User_Exit);
        progressBar = (ProgressBar) view.findViewById(R.id.progresBar);
        btnEnter_User = (Button) view.findViewById(R.id.btnEnter_User2);
        btnExit_User = (Button) view.findViewById(R.id.btnExit_User);
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


            ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.INTERNET}, 1);



        }
        Location location = locationManager.getLastKnownLocation(locationManager.NETWORK_PROVIDER);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                firebaseAuth.signOut();
                startActivity(new Intent(getActivity(), com.mogkt.mogkt.Login.login_manager.class));
            }
        });


        onLocationChanged(location);

        btnEnter_User.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnEnter_User.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                Toast.makeText(getActivity() ,"Giriş başarılı", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });






        return view;

    }
    public void saveDateForUser(){

        String date;
        Calendar calendar =Calendar.getInstance();
        date = DateFormat.getDateInstance().format(calendar.getTime()).toString().trim();
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        String time = format.format(calendar.getTime());
        String email = FirebaseAuth.getInstance().getCurrentUser().getEmail().toString().trim();
        com.mogkt.mogkt.Admin.enter_exit_DB enter_exit_db = new com.mogkt.mogkt.Admin.enter_exit_DB
                (email,date,time );

        databaseReference.push().setValue(enter_exit_db);




    }

    @Override
    public void onLocationChanged(Location location) {

        final double longitude = location.getLongitude();
        final double latitude = location.getLatitude();
        textView_user.setText(""+longitude+" "+latitude);
        btnExit_User.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (29.04<longitude || 29.06>longitude && 41.09<latitude || 41.1>latitude){
                    Toast.makeText(getActivity(), "Çıkış Başarılı", Toast.LENGTH_LONG).show();

                    saveDateForUser();
                    btnExit_User.setEnabled(false);
                }
                else
                    Toast.makeText(getActivity(), "Çıkış Başarısız", Toast.LENGTH_LONG).show();

            }
        });


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
