package com.mogkt.mogkt.Users.Enter;

import android.Manifest;
import android.app.Fragment;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mogkt.mogkt.R;

/**
 * Created by alper_yilmaz_111 on 3.03.2018.
 */

public class User_Home_Fragment_Enter extends Fragment implements LocationListener {

    private TextView textView_user;
    private LocationManager locationManager;
    private Button btnEnter_User;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_home_enter, null);
        textView_user = (TextView) view.findViewById(R.id.lastEnter_User_Enter);
        btnEnter_User = (Button) view.findViewById(R.id.btnEnter_User);
        ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.INTERNET}, 1);
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


        }
        Location location = locationManager.getLastKnownLocation(locationManager.NETWORK_PROVIDER);


        onLocationChanged(location);


        return view;

    }

    @Override
    public void onLocationChanged(Location location) {

        final double longitude = location.getLongitude();
        final double latitude = location.getLatitude();
        textView_user.setText(""+longitude+" "+latitude);
        btnEnter_User.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (29.04<longitude || 29.06>longitude && 41.09<latitude || 41.1>latitude){
                    Toast.makeText(getActivity(), "Giriş Başarılı", Toast.LENGTH_LONG).show();
                    btnEnter_User.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(getActivity(), "Bir kere giriş yapmanız yeterli", Toast.LENGTH_LONG).show();
                        }
                    });
                }
                else
                    Toast.makeText(getActivity(), "Giriş Başarısız", Toast.LENGTH_LONG).show();

            }
        });


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
