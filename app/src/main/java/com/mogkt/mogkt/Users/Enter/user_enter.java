package com.mogkt.mogkt.Users.Enter;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.mogkt.mogkt.Admin.Report_Admin_Fragment;
import com.mogkt.mogkt.Admin.Settings_Admin_Fragment;
import com.mogkt.mogkt.R;
import com.mogkt.mogkt.Users.Report_User_Fragment;
import com.mogkt.mogkt.Users.Settings_User_Fragment;

/**
 * Created by alper_yilmaz_111 on 3.03.2018.
 */

public class user_enter extends AppCompatActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_enter);


        BottomNavigationView navigation = findViewById(R.id.navigation_user_enter);
        navigation.setOnNavigationItemSelectedListener(this);

        loadFragment(new User_Home_Fragment_Enter());
    }

    private boolean loadFragment (Fragment fragment) {

        if (fragment != null) {

            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_Container_User, fragment)
                    .commit();

            return true;
        }

        return false;

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Fragment fragment = null;

        switch (item.getItemId()){

            case R.id.navigation_home_user:
                fragment = new User_Home_Fragment_Enter();
                break;

            case R.id.navigation_report_user:
                fragment = new Report_User_Fragment();
                break;

            case R.id.navigation_settings_user:
                fragment = new Settings_User_Fragment();
                break;

        }


        return loadFragment(fragment);
    }
}



