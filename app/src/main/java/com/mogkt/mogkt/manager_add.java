package com.mogkt.mogkt;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mogkt.mogkt.Login.forgetPassword;
import com.mogkt.mogkt.Login.login_manager;
import com.mogkt.mogkt.Login.registerDB;


public class manager_add extends Fragment implements View.OnClickListener {

    EditText editTextName, editTextEmail, editTextPassword, editTextPasswordCheck;
    FirebaseAuth mAuth;
    ProgressBar progressBar;
    Spinner spinner_job;
    Button btn_register;
    TextView btnBackToLogin, btnForgetPassword;
    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_manager_add, null);

        firebaseAuth = FirebaseAuth.getInstance();

        databaseReference = FirebaseDatabase.getInstance().getReference("users");

        FirebaseUser user = firebaseAuth.getCurrentUser();


        //View
        editTextName = (EditText) view.findViewById(R.id.manager_add_name);
        editTextEmail = (EditText) view.findViewById(R.id.manager_add_email);
        editTextPassword = (EditText) view.findViewById(R.id.manager_add_pass);
        editTextPasswordCheck = (EditText) view.findViewById(R.id.manager_add_cpass);
        progressBar = (ProgressBar) view.findViewById(R.id.progresBar2);
        spinner_job = (Spinner) view.findViewById(R.id.manager_add_spinner_job);
        btn_register = (Button) view.findViewById(R.id.manager_add_register);

        btn_register.setOnClickListener((View.OnClickListener) this);

        //init firebase

        mAuth = FirebaseAuth.getInstance();


        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){


            case R.id.manager_add_register:
                signUpUser(editTextEmail.getText().toString(),editTextPassword.getText().toString());
                addUser();

        }

    }
    private void signUpUser(String email, String password) {

        progressBar.setVisibility(View.VISIBLE);

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        progressBar.setVisibility(View.GONE);

                        if (!task.isSuccessful()){

                            Toast.makeText(getActivity(),"Hata: "+task.getException(), Toast.LENGTH_SHORT).show();
                        }
                        else {

                            Toast.makeText(getActivity(),"Kayıt Başarılı: ", Toast.LENGTH_SHORT).show();
                            editTextName.setText("");
                            editTextEmail.setText("");
                            editTextPassword.setText("");
                            editTextPasswordCheck.setText("");



                        }
                    }
                });
    }

    private void addUser(){

        String name = editTextName.getText().toString().trim();
        String email = editTextEmail.getText().toString().trim();
        String job = spinner_job.getSelectedItem().toString().trim();
        String password = editTextPassword.getText().toString().trim();

        registerDB db = new registerDB(name,email,job,password);

        String id = databaseReference.push().getKey();



        FirebaseUser user = mAuth.getCurrentUser();

        databaseReference.child(id).setValue(db);

        Toast.makeText(getActivity(),id+"", Toast.LENGTH_LONG).show();


    }

}
