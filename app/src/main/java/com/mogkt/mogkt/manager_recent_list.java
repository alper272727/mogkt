package com.mogkt.mogkt;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mogkt.mogkt.Login.registerDB;

import java.util.ArrayList;


public class manager_recent_list extends Fragment {

    ListView listView;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    ArrayList<String> list;
    ArrayAdapter<String> adapter;
    com.mogkt.mogkt.Login.registerDB registerDB;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_manager_recent_list, null);

        listView = (ListView) view.findViewById(R.id.listViewRecent);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("users");

        list = new ArrayList<>();
        adapter = new ArrayAdapter<String>(getContext(),R.layout.userinfotext, R.id.nameInfotext,list);


        registerDB = new registerDB();

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds: dataSnapshot.getChildren()){

                    registerDB = ds.getValue(com.mogkt.mogkt.Login.registerDB.class);
                    list.add(registerDB.getJobID().toString()+ " \n" + registerDB.getEmailID().toString()+"\n"
                            +registerDB.getNameID().toString());

                }
                listView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        return view;
    }

}
