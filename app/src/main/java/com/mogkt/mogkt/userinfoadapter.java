package com.mogkt.mogkt;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.mogkt.mogkt.Login.registerDB;

import java.util.ArrayList;
import java.util.List;

public class userinfoadapter extends ArrayAdapter<String>{

    private Activity context;
    private List<String> liste;
    DatabaseReference databaseReference;
    ImageButton deleteU;
    TextView textView;
    ViewGroup parent;

    public userinfoadapter(@NonNull Activity context, TextView textView, ArrayList<String> liste,
                           DatabaseReference databaseReference) {


        super(context, R.layout.userinfo,R.id.nameInfo,liste);
        this.context=context;
        this.textView = textView;

        //this.deleteU=deleteU;
        this.liste=liste;
        this.databaseReference=databaseReference;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View viewList = inflater.inflate(R.layout.userinfo,null,true);
        deleteU = (ImageButton) viewList.findViewById(R.id.deleteUser);
        deleteU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(),"Silme Yeri",Toast.LENGTH_LONG).show();
            }
        });

        return viewList;

    }
}
