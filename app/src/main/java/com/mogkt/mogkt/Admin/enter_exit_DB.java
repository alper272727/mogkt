package com.mogkt.mogkt.Admin;

public class enter_exit_DB {

    public String email,date,time;

    public enter_exit_DB(String email, String date, String time) {
        this.email = email;
        this.date = date;
        this.time = time;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
