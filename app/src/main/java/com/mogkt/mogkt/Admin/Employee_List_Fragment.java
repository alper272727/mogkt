package com.mogkt.mogkt.Admin;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.mogkt.mogkt.Admin.Enter.Admin_Home_Fragment_Enter;
import com.mogkt.mogkt.R;

/**
 * Created by alper_yilmaz_111 on 3.03.2018.
 */

public class Employee_List_Fragment extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_admin_employee_list, null);

        BottomNavigationView navigation = view.findViewById(R.id.navigation_user_list);

        navigation.setOnNavigationItemSelectedListener(this);

        loadFragment(new com.mogkt.mogkt.manager_recent_list());



        return view;

    }


    private boolean loadFragment (Fragment fragment) {

        if (fragment != null) {

            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_employee_list, fragment)
                    .commit();

            return true;
        }

        return false;

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;

        switch (item.getItemId()){

            case R.id.liste:
                fragment = new com.mogkt.mogkt.manager_recent_list();
                break;

            case R.id.add:
                fragment = new com.mogkt.mogkt.manager_add();
                break;

            case R.id.delete:
                fragment = new com.mogkt.mogkt.manager_delete();
                break;

            case R.id.edit:
                fragment = new com.mogkt.mogkt.manager_edit();
                break;
            case R.id.exit:
                fragment = new com.mogkt.mogkt.manager_exit();
                break;

        }


        return loadFragment(fragment);
    }

    }

