package com.mogkt.mogkt.Admin;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.arch.lifecycle.ReportFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.view.CollapsibleActionView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mogkt.mogkt.Admin.Enter.Admin_Home_Fragment_Enter;
import com.mogkt.mogkt.Login.registerDB;
import com.mogkt.mogkt.R;
import com.mogkt.mogkt.Users.Report_User_Fragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by alper_yilmaz_111 on 3.03.2018.
 */

public class Report_Admin_Fragment extends Fragment {


    TextView tw, tw2,tw3;
    Button btn_baslangic_tarih, btn_bitis_tarih,selectUser;
    int year, month, day;
    Calendar calendar;
    Dialog myDialog;
    ListView listView;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    ArrayList<String> list;
    ArrayAdapter<String> adapter;
    registerDB registerDB;





    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, Bundle savedInstanceState) {


        final View view = inflater.inflate(R.layout.fragment_admin_report, null);

        myDialog = new Dialog(getContext());




        selectUser = (Button) view.findViewById(R.id.users);
        tw = (TextView) view.findViewById(R.id.text_baslangic);
        btn_baslangic_tarih = (Button) view.findViewById(R.id.baslangic_tarih);
        tw2 = (TextView) view.findViewById(R.id.text_bitis);
        btn_bitis_tarih = (Button) view.findViewById(R.id.bitis_tarih);

        calendar = Calendar.getInstance();
        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);

        btn_baslangic_tarih.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        month = month+1;
                        tw.setText(dayOfMonth+"/"+month+"/"+year);

                    }
                },year,month,day);
                datePickerDialog.show();

            }
        });
        btn_bitis_tarih.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        month = month+1;
                        tw2.setText(dayOfMonth+"/"+month+"/"+year);
                    }
                },year,month,day);
                datePickerDialog.show();

            }
        });



        selectUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopUp(view);


            }
        });


        return view;
    }

    public void showPopUp(View view){
        TextView btnClose,save;


        save = (TextView) myDialog.findViewById(R.id.saveBtn);



        myDialog.setContentView(R.layout.userpopup);
        btnClose = (TextView) myDialog.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.show();
        listView = (ListView) myDialog.findViewById(R.id.listViewPopUp);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("users");

        list = new ArrayList<>();
        adapter = new ArrayAdapter<String>(getContext(),R.layout.userinfocheck, R.id.checkBox,list);

        registerDB = new registerDB();

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds: dataSnapshot.getChildren()){

                    registerDB = ds.getValue(com.mogkt.mogkt.Login.registerDB.class);
                    list.add(registerDB.getJobID().toString()+ " \n" + registerDB.getEmailID().toString()+"\n"
                            +registerDB.getNameID().toString());

                }
                listView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }



}




