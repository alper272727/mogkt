package com.mogkt.mogkt.Admin.Enter;

import android.Manifest;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mogkt.mogkt.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static android.content.Intent.getIntent;

/**
 * Created by alper_yilmaz_111 on 3.03.2018.
 */

public class Admin_Home_Fragment_Enter extends Fragment implements LocationListener {


    private TextView textView,textView2,textView3,textView4;
    private LocationManager locationManager;
    private Button btnEnter;
    ImageButton btnExitToApp;
    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference, databaseReferenceExit;
    FirebaseAuth.AuthStateListener authStateListener;
    static final int REQUEST_LOCATION = -1;







    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin_home_enter, null);

        firebaseAuth = FirebaseAuth.getInstance();
        /*if (firebaseAuth.getCurrentUser() == null){

            startActivity(new Intent(getActivity(), com.mogkt.mogkt.Login.login_manager.class));
        }
        */

        databaseReference = FirebaseDatabase.getInstance().getReference("enterTable");





        textView4 = (TextView) view.findViewById(R.id.textView7);
        textView = (TextView) view.findViewById(R.id.lastEnter_Admin_Enter);
        btnEnter = (Button) view.findViewById(R.id.btnEnter_Admin);
        btnExitToApp = (ImageButton) view.findViewById(R.id.btnExit_Admin_Exit);
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {



            ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            //ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            //ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.INTERNET}, 1);

        }
        Location location = locationManager.getLastKnownLocation(locationManager.NETWORK_PROVIDER);

        onLocationChanged(location);

        btnExitToApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              firebaseAuth.signOut();
              startActivity(new Intent(getActivity(), com.mogkt.mogkt.Login.login_manager.class));
            }
        });

        textView2 = (TextView) view.findViewById(R.id.textView5);
        textView3 = (TextView) view.findViewById(R.id.textView6);

        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in

                    Toast.makeText(getActivity(), user.getEmail()+" Hoşgeldiniz.", Toast.LENGTH_SHORT).show();
                    textView4.setText(user.getEmail());
                }
            }
        };





        return view;

    }
    @Override
    public void onStart(){
        super.onStart();
        firebaseAuth.addAuthStateListener(authStateListener);

    }


    public void saveDateForUser(){

        String date;
        Calendar calendar =Calendar.getInstance();
        date = DateFormat.getDateInstance().format(calendar.getTime()).toString().trim();
        textView2.setText(date);
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        String time = format.format(calendar.getTime());
        textView3.setText(time);
        String email = FirebaseAuth.getInstance().getCurrentUser().getEmail().toString().trim();
        com.mogkt.mogkt.Admin.enter_exit_DB enter_exit_db = new com.mogkt.mogkt.Admin.enter_exit_DB
                (email,date,time );

        databaseReference.push().setValue(enter_exit_db);




    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case REQUEST_LOCATION:
            
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        final double longitude = location.getLongitude();
        final double latitude = location.getLatitude();
        textView.setText(""+longitude+" "+latitude);
        btnEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (29.04<longitude || 29.06>longitude && 41.09<latitude || 41.1>latitude){
                    Toast.makeText(getActivity(), "Giriş Başarılı", Toast.LENGTH_LONG).show();
                    saveDateForUser();


                    btnEnter.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(getActivity(), "Çıkış sayfasına yönlendiriliyorsun", Toast.LENGTH_LONG).show();


                            startActivity(new Intent(getActivity(), com.mogkt.mogkt.Users.Exit.user_exit.class));

                        }
                    });
                }
                else
                    Toast.makeText(getActivity(), "Giriş Başarısız", Toast.LENGTH_LONG).show();

            }
        });



    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


}
