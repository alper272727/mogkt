package com.mogkt.mogkt.Admin.Exit;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.mogkt.mogkt.Admin.Employee_List_Fragment;
import com.mogkt.mogkt.Admin.Enter.Admin_Home_Fragment_Enter;
import com.mogkt.mogkt.Admin.Report_Admin_Fragment;
import com.mogkt.mogkt.Admin.Settings_Admin_Fragment;
import com.mogkt.mogkt.R;

/**
 * Created by alper_yilmaz_111 on 3.03.2018.
 */

public class admin_exit extends AppCompatActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_exit);


        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);

        loadFragment(new Admin_Home_Fragment_Enter());
    }

    private boolean loadFragment (Fragment fragment) {

        if (fragment != null) {

            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_Container, fragment)
                    .commit();

            return true;
        }

        return false;

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Fragment fragment = null;

        switch (item.getItemId()){

            case R.id.navigation_home:
                fragment = new Home_Fragment_Exit();
                break;

            case R.id.navigation_report:
                fragment = new Report_Admin_Fragment();
                break;

            case R.id.navigation_employeeList:
                fragment = new Employee_List_Fragment();
                break;

            case R.id.navigation_settings:
                fragment = new Settings_Admin_Fragment();
                break;

        }


        return loadFragment(fragment);
    }
}


