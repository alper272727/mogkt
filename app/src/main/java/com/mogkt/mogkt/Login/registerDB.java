package com.mogkt.mogkt.Login;

public class registerDB {

    String nameID,emailID,jobID,passwordID;

    public registerDB() {

    }

    public registerDB(String nameID,String emailID, String jobID, String passwordID) {
        this.nameID = nameID;
        this.emailID = emailID;
        this.jobID = jobID;
        this.passwordID = passwordID;
    }


    public String getNameID() {
        return nameID;
    }
    public String getEmailID() {
        return emailID;
    }

    public String getJobID() {
        return jobID;
    }

    public String getPasswordID() {
        return passwordID;
    }



    public void setNameID(String nameID) {
        this.nameID = nameID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public void setJobID(String jobID) {
        this.jobID = jobID;
    }

    public void setPasswordID(String passwordID) {
        this.passwordID = passwordID;
    }
}
