package com.mogkt.mogkt.Login;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ThrowOnExtraProperties;
import com.google.firebase.database.ValueEventListener;
import com.mogkt.mogkt.Admin.Enter.admin_enter;
import com.mogkt.mogkt.R;
import com.mogkt.mogkt.Users.Enter.User_Home_Fragment_Enter;


public class login_manager_fragment extends Fragment implements View.OnClickListener {


    Button btnSignIn;
    EditText editTextEmail, editTextPassword;
    TextView btnRegister_manager, btnForgetPassword, textView;
    FirebaseAuth mAuth;
    ProgressBar progressBar;
    FirebaseAuth.AuthStateListener authStateListener;
    FirebaseDatabase db = FirebaseDatabase.getInstance();
    DatabaseReference databaseReference = db.getReference();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_manager, null);

        //View

        databaseReference = FirebaseDatabase.getInstance().getReference("users");

        textView = (TextView) view.findViewById(R.id.textView9);
        //textView = (TextView) view.findViewById(R.id.textView8);
        btnSignIn = (Button) view.findViewById(R.id.btn_login_manager);
        editTextEmail = (EditText) view.findViewById(R.id.editText_email_manager);
        editTextPassword = (EditText) view.findViewById(R.id.editText_password_manager);
        btnRegister_manager = (TextView) view.findViewById(R.id.register_manager);
        btnForgetPassword = (TextView) view.findViewById(R.id.forgetPassword);
        progressBar = (ProgressBar) view.findViewById(R.id.progresBar);

        btnRegister_manager.setPaintFlags(btnRegister_manager.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        btnRegister_manager.setMovementMethod(LinkMovementMethod.getInstance());


        btnForgetPassword.setPaintFlags(btnForgetPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        btnForgetPassword.setMovementMethod(LinkMovementMethod.getInstance());









        //onclick

        btnSignIn.setOnClickListener((View.OnClickListener) this);
        btnRegister_manager.setOnClickListener((View.OnClickListener) this);
        btnForgetPassword.setOnClickListener((View.OnClickListener) this);

        //init firabase check this
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {


                }
            }
        };



            mAuth = FirebaseAuth.getInstance();

            if(mAuth.getCurrentUser() !=null){
            startActivity(new Intent(getActivity(), admin_enter.class));
        }


        return view;

    }


    public void onStart(){
        super.onStart();
        mAuth.addAuthStateListener(authStateListener);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.register_manager:
                startActivity(new Intent(getActivity(), register.class));
                break;
            case R.id.forgetPassword:
                startActivity(new Intent(getActivity(), forgetPassword.class));
                break;
            case R.id.btn_login_manager:
                loginUser(editTextEmail.getText().toString(), editTextPassword.getText().toString());

        }

    }


    private void loginUser(final String email, final String password) {

        progressBar.setVisibility(View.VISIBLE);

        String id = databaseReference.push().getKey();

        DatabaseReference okuma = FirebaseDatabase.getInstance().getReference().child("users").child("-LAptcsLwJLHqr51x0cJ");


        ValueEventListener dinle = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                registerDB registerDB = new registerDB();
                registerDB = dataSnapshot.getValue(com.mogkt.mogkt.Login.registerDB.class);

                String text = registerDB.getJobID();
                textView.setText(text);
                if (text.equals("Genel Müdür")){
                    mAuth.signInWithEmailAndPassword(email, password)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {

                                    progressBar.setVisibility(View.GONE);
                                    if (!task.isSuccessful()) {

                                        Toast.makeText(getActivity(), "Kullanıcı adı veya şifre yanlış. İşçi arayüzünden girmeyi de dene", Toast.LENGTH_SHORT).show();
                                    } else {


                                        startActivity(new Intent(getActivity(), com.mogkt.mogkt.Admin.Enter.admin_enter.class));
                                    }
                                }
                            });
                }
                else {
                    Toast.makeText(getActivity(), "İşçi arayüzünden girmeyi de dene", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        okuma.addValueEventListener(dinle);
    }
}
