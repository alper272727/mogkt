package com.mogkt.mogkt.Login;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.mogkt.mogkt.R;

public class forgetPassword extends AppCompatActivity implements View.OnClickListener {

    EditText editTextEmail;
    Button btnReset, btnBack;
    FirebaseAuth mAuth;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_ppassword);

        //View
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        btnReset = (Button) findViewById(R.id.btnReset);
        btnBack = (Button) findViewById(R.id.btnBack);
        progressBar = (ProgressBar) findViewById(R.id.progresBar3);

        //onclick

        btnReset.setOnClickListener((View.OnClickListener) this);
        btnBack.setOnClickListener((View.OnClickListener) this);

        mAuth = FirebaseAuth.getInstance();


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnBack:
                startActivity(new Intent(this, login_manager.class));
                break;
            case R.id.btnReset:
                resetPassword(editTextEmail.getText().toString());
                break;
        }
    }

    private void resetPassword(final String email) {

        progressBar.setVisibility(View.VISIBLE);


        mAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        progressBar.setVisibility(View.GONE);

                        if (task.isSuccessful()){
                            Toast.makeText(getApplicationContext(),email+" adresine doğrulama için mail gönderdik.", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(getApplicationContext(),email+" adresini maalesef bulamadık.", Toast.LENGTH_SHORT).show();


                        }


                    }
                });
    }
}
