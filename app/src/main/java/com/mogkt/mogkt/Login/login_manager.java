package com.mogkt.mogkt.Login;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.mogkt.mogkt.R;

public class login_manager extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Fragment fragment = null;

        switch (item.getItemId()){

            case R.id.navigation_manager:
                fragment = new login_manager_fragment();
                break;

            case R.id.navigation_employee:
                fragment = new login_employee_fragment();
                break;
        }


        return loadFragment(fragment);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_manager);
        BottomNavigationView navigation = findViewById(R.id.navigation_login);
        navigation.setOnNavigationItemSelectedListener(this);

        loadFragment(new login_manager_fragment());


    }

    private boolean loadFragment (Fragment fragment) {

        if (fragment != null) {

            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_login, fragment)
                    .commit();

            return true;
        }

        return false;
    }




}
