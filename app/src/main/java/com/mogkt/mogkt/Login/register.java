package com.mogkt.mogkt.Login;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mogkt.mogkt.R;

import java.util.regex.Pattern;

public class register extends AppCompatActivity implements View.OnClickListener {


    EditText editTextName, editTextEmail, editTextPassword, editTextPasswordCheck;
    FirebaseAuth mAuth;
    ProgressBar progressBar;
    Spinner spinner_job;
    Button btn_register;
    TextView btnBackToLogin, btnForgetPassword;
    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        firebaseAuth = FirebaseAuth.getInstance();

        databaseReference = FirebaseDatabase.getInstance().getReference("users");

        FirebaseUser user = firebaseAuth.getCurrentUser();


        //View
        editTextName = (EditText) findViewById(R.id.name);
        editTextEmail = (EditText) findViewById(R.id.email);
        editTextPassword = (EditText) findViewById(R.id.pass);
        editTextPasswordCheck = (EditText) findViewById(R.id.cpass);
        progressBar = (ProgressBar) findViewById(R.id.progresBar2);
        spinner_job = (Spinner) findViewById(R.id.spinner_job);
        btn_register = (Button) findViewById(R.id.register);
        btnBackToLogin = (TextView) findViewById(R.id.btnBackToLogin);
        btnForgetPassword = (TextView) findViewById(R.id.forgetPassword_register);

        //Click

        btn_register.setOnClickListener((View.OnClickListener) this);
        btnBackToLogin.setOnClickListener((View.OnClickListener) this);
        btnForgetPassword.setOnClickListener((View.OnClickListener) this);

        //init firebase

        mAuth = FirebaseAuth.getInstance();



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.btnBackToLogin:
                startActivity(new Intent(this, login_manager.class));
                break;
            case R.id.forgetPassword_register:
                startActivity(new Intent(this, forgetPassword.class));
                break;
            case R.id.register:
                signUpUser(editTextEmail.getText().toString(),editTextPassword.getText().toString());
                addUser();

        }

    }

    private void signUpUser(String email, String password) {

        progressBar.setVisibility(View.VISIBLE);

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        progressBar.setVisibility(View.GONE);

                        if (!task.isSuccessful()){

                            Toast.makeText(getApplicationContext(),"Hata: "+task.getException(), Toast.LENGTH_SHORT).show();
                        }
                        else {

                            Toast.makeText(getApplicationContext(),"Kayıt Başarılı: ", Toast.LENGTH_SHORT).show();
                            editTextName.setText("");
                            editTextEmail.setText("");
                            editTextPassword.setText("");
                            editTextPasswordCheck.setText("");



                        }
                    }
                });
    }

    private void addUser(){

        String name = editTextName.getText().toString().trim();
        String email = editTextEmail.getText().toString().trim();
        String job = spinner_job.getSelectedItem().toString().trim();
        String password = editTextPassword.getText().toString().trim();

            registerDB db = new registerDB(name,email,job,password);

            String id = databaseReference.push().getKey();



        FirebaseUser user = mAuth.getCurrentUser();

            databaseReference.child(id).setValue(db);

        Toast.makeText(getApplicationContext(),id+"", Toast.LENGTH_LONG).show();




    }



}


