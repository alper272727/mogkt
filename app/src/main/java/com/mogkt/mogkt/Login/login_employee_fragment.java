package com.mogkt.mogkt.Login;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mogkt.mogkt.Admin.Enter.admin_enter;
import com.mogkt.mogkt.R;

/**
 * Created by alper_yilmaz_111 on 24.03.2018.
 */

public class login_employee_fragment extends Fragment implements View.OnClickListener {

    TextView register_user,forgetPassword;
    EditText editTextMail,editTextPassword;
    Button btn_login;
    FirebaseAuth firebaseAuth;
    ProgressBar progressBar;
    FirebaseAuth.AuthStateListener authStateListener;

    DatabaseReference databaseReference;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_login_employee, null);

        databaseReference = FirebaseDatabase.getInstance().getReference("users");

        firebaseAuth = FirebaseAuth.getInstance();

        forgetPassword = (TextView) view.findViewById(R.id.forgetPasswordEmployee);
        forgetPassword.setPaintFlags(forgetPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        forgetPassword.setMovementMethod(LinkMovementMethod.getInstance());

        register_user = (TextView) view.findViewById(R.id.register_user);
        register_user.setPaintFlags(register_user.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        register_user.setMovementMethod(LinkMovementMethod.getInstance());

        editTextMail = (EditText) view.findViewById(R.id.editText_email_user);
        editTextPassword = (EditText) view.findViewById(R.id.editText_password_user);
        progressBar = (ProgressBar) view.findViewById(R.id.progresBar_user);

        btn_login = (Button) view.findViewById(R.id.btn_login_user);
        forgetPassword.setOnClickListener((View.OnClickListener) this);
        register_user.setOnClickListener((View.OnClickListener) this);
        btn_login.setOnClickListener((View.OnClickListener) this);
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {


                }
            }
        };




        if(firebaseAuth.getCurrentUser() !=null){
            startActivity(new Intent(getActivity(), admin_enter.class));
        }
        return view;

    }

    public void onStart(){
        super.onStart();
        firebaseAuth.addAuthStateListener(authStateListener);


    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.register_user:
                startActivity(new Intent(getActivity(), register.class));
                break;

            case R.id.forgetPasswordEmployee:
                startActivity(new Intent(getActivity(), forgetPassword.class));
                break;

            case R.id.btn_login_user:
                loginUser_User(editTextMail.getText().toString(), editTextPassword.getText().toString());



        }
    }

    private void loginUser_User(String email, String password) {

        progressBar.setVisibility(View.VISIBLE);

        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        progressBar.setVisibility(View.GONE);
                        if (!task.isSuccessful()) {

                            Toast.makeText(getActivity(), "Kullanıcı adı veya şifre yanlış. İşçi arayüzünden girmeyi de dene", Toast.LENGTH_SHORT).show();
                        } else {


                            startActivity(new Intent(getActivity(), com.mogkt.mogkt.Admin.Enter.admin_enter.class));
                        }
                    }});}}