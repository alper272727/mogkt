package com.mogkt.mogkt;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mogkt.mogkt.Login.registerDB;

import java.security.PublicKey;
import java.util.ArrayList;

import javax.xml.transform.Templates;


public class manager_delete extends Fragment {


    ListView listView;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    ArrayList<String> list;
    ArrayAdapter<String> adapter;
    com.mogkt.mogkt.Login.registerDB registerDB;
    ImageButton deleteUser;
    TextView textView;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_manager_delete, null);
        View view2 = inflater.inflate(R.layout.userinfo, null);


        textView = (TextView) view.findViewById(R.id.nameInfo);

        listView = (ListView) view.findViewById(R.id.listViewDelete);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("users");

        list = new ArrayList<>();

        final userinfoadapter userinfoadapter = new userinfoadapter(getActivity(),textView,list,databaseReference);
        //adapter = new ArrayAdapter<String>(getContext(),R.layout.userinfo, R.id.nameInfo,list);




        registerDB = new registerDB();

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds: dataSnapshot.getChildren()){

                    registerDB = ds.getValue(com.mogkt.mogkt.Login.registerDB.class);
                    list.add(registerDB.getJobID().toString()+ " \n" + registerDB.getEmailID().toString()+"\n"
                            +registerDB.getNameID().toString());

                }
                listView.setAdapter(userinfoadapter);
            }



            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        return view;
    }





}
