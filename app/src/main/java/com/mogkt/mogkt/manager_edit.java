package com.mogkt.mogkt;

import android.app.Dialog;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mogkt.mogkt.Login.registerDB;

import java.util.ArrayList;


public class manager_edit extends Fragment {


    ListView listView;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    ArrayList<String> list;
    ArrayAdapter<String> adapter;
    com.mogkt.mogkt.Login.registerDB registerDB;
    ImageButton deleteUser;
    TextView textView;
    Dialog myDialog;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_manager_edit, null);
        textView = (TextView) view.findViewById(R.id.nameInfotedit);

        myDialog = new Dialog(getContext());

        listView = (ListView) view.findViewById(R.id.listViewEdit);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("users");

        list = new ArrayList<>();

        final userinfoadapterEdit userinfoadapter = new userinfoadapterEdit(getActivity(),textView,list,databaseReference);
        //adapter = new ArrayAdapter<String>(getContext(),R.layout.userinfo, R.id.nameInfo,list);




        registerDB = new registerDB();

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds: dataSnapshot.getChildren()){

                    registerDB = ds.getValue(com.mogkt.mogkt.Login.registerDB.class);
                    list.add(registerDB.getJobID().toString()+ " \n" + registerDB.getEmailID().toString()+"\n"
                            +registerDB.getNameID().toString());

                }
                listView.setAdapter(userinfoadapter);
            }



            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        return view;
    }
    public void showPopUp(View view){
        TextView btnClose,save;


        save = (TextView) myDialog.findViewById(R.id.register_edit);



        myDialog.setContentView(R.layout.editpopup);
        btnClose = (TextView) myDialog.findViewById(R.id.dismis);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.show();
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });






    }



}
