package com.mogkt.mogkt;

import android.app.Activity;
import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.List;

public class userinfoadapterEdit extends ArrayAdapter<String>{

    private Activity context;
    private List<String> liste;
    DatabaseReference databaseReference;
    ImageButton editBtn;
    TextView textView;
    ViewGroup parent;
    Dialog myDialog;
    TextView btnClose;
    Button btnRegister;

    public userinfoadapterEdit(@NonNull Activity context, TextView textView, ArrayList<String> liste,
                               DatabaseReference databaseReference) {


        super(context, R.layout.userinfoedit,R.id.nameInfotedit,liste);
        this.context=context;
        this.textView = textView;

        //this.deleteU=deleteU;
        this.liste=liste;
        this.databaseReference=databaseReference;

    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        myDialog = new Dialog(getContext());
        btnRegister = (Button) myDialog.findViewById(R.id.register_edit);



        LayoutInflater inflater = context.getLayoutInflater();
        final View viewList = inflater.inflate(R.layout.userinfoedit,null,true);
        editBtn = (ImageButton) viewList.findViewById(R.id.editBtn);
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getContext(),"Edit yeri",Toast.LENGTH_LONG).show();

            }
        });


        return viewList;

    }

    /*public void showPopUp(View view){


        btnRegister = (Button) myDialog.findViewById(R.id.register_edit);

        myDialog.setContentView(R.layout.editpopup);
        btnClose = (TextView) myDialog.findViewById(R.id.dismis);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.show();
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });






    }*/
}
